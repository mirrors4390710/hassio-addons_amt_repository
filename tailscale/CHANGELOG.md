## What’s changed

## 🐛 Bug fixes

- 🐛 Bugfix in migrated S6 scripts @lmagyar ([#158](https://github.com/hassio-addons/addon-tailscale/pull/158))

## 🚀 Enhancements

- 🚀 Advertise all supported interfaces as Tailscale Subnets @lmagyar ([#135](https://github.com/hassio-addons/addon-tailscale/pull/135))
- 🚀 Suppress tailscaled logs after 200 lines @lmagyar ([#138](https://github.com/hassio-addons/addon-tailscale/pull/138))

## 📚 Documentation

- 📚 Fix broken URL @wrt54g ([#159](https://github.com/hassio-addons/addon-tailscale/pull/159))

## ⬆️ Dependency updates

- ⬆️ Update Add-on base image to v13.1.5 @renovate ([#160](https://github.com/hassio-addons/addon-tailscale/pull/160))
- ⬆️ Update tailscale/tailscale to v1.38.3 @renovate ([#161](https://github.com/hassio-addons/addon-tailscale/pull/161))
- ⬆️ Update Add-on base image to v13.2.0 @renovate ([#163](https://github.com/hassio-addons/addon-tailscale/pull/163))
