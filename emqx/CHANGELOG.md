## What’s changed

## 🚀 Enhancements

- Limit maximum open file handles for EMQX specifically @agners ([#38](https://github.com/hassio-addons/addon-emqx/pull/38))

## 📚 Documentation

- Fix broken URL @wrt54g ([#37](https://github.com/hassio-addons/addon-emqx/pull/37))
